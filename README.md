# Prelottie (Android)

## The companion app for [Prelottie Web](https://github.com/kimar/prelottie-web).

![Logo](https://drive.google.com/uc?export=download&id=1D67WBfMFFw7qa5RWquhqwX5Jq2_JPsr4)

I created this project to learn about Ktor, the Kotlin Web Framework. It's a viewer for Bodymovin / Lottie files and can be found [here in the Play Store](https://play.google.com/store/apps/details?id=io.kida.lottieprelude).

## License

See [LICENSE.md](LICENSE.md).
