package io.kida.lottieprelude

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import io.kida.lottieprelude.api.ApiConnector
import io.kida.lottieprelude.extension.*
import io.kida.lottieprelude.info.fragment.InfoFragment
import io.kida.lottieprelude.preview.fragment.PreviewFragment
import io.kida.lottieprelude.scanner.fragment.CameraPermissionFragment
import io.kida.lottieprelude.scanner.fragment.ScanResultFragment
import io.kida.lottieprelude.scanner.fragment.ScannerFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.launch
import java.io.File

class MainActivity : AppCompatActivity(),
        CameraPermissionFragment.Callbacks,
        ScannerFragment.Callbacks,
        ScanResultFragment.Callbacks {

    private val apiConnector = ApiConnector()
    private var downloadDialog: AlertDialog? = null

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_scan -> {
                defaultSharedPreferences().getCode()?.let {
                    val fragment = ScanResultFragment()
                    fragment.listener = this
                    supportFragmentManager.replaceMain(fragment)
                    return@OnNavigationItemSelectedListener true
                }
                val permissionFragment = CameraPermissionFragment()
                permissionFragment.permissionListener = this
                supportFragmentManager.replaceMain(permissionFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_preview -> {
                val fragment = PreviewFragment()
                supportFragmentManager.beginTransaction()
                        .replace(R.id.main_fragment, fragment)
                        .commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_info -> {
                val fragment = InfoFragment()
                supportFragmentManager.beginTransaction()
                        .replace(R.id.main_fragment, fragment)
                        .commit()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.title = getString(R.string.app_title)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        if (supportFragmentManager.fragments.isEmpty()) {
            val permissionFragment = CameraPermissionFragment()
            permissionFragment.permissionListener = this
            supportFragmentManager.beginTransaction()
                    .add(R.id.main_fragment, permissionFragment)
                    .commit()
        } else {
            supportFragmentManager.fragments
                    .filterAndMap<CameraPermissionFragment>()
                    .forEach { it.permissionListener = this }
        }

    }

    override fun onScanSuccess(code: String) {
        defaultSharedPreferences().setCode(code)

        val fragment = ScanResultFragment()
        fragment.listener = this

        supportFragmentManager.beginTransaction()
                .replace(R.id.main_fragment, fragment)
                .commit()

        // download files
        downloadDialog = AlertDialog.Builder(this)
                .setTitle(R.string.downloading_title)
                .setMessage("Downloading Descriptor")
                .setCancelable(false)
                .create()

        downloadDialog?.show()

        launch(CommonPool) {
            val descriptor = apiConnector.getDescriptor(code) ?: return@launch
            descriptor.files.forEach {
                runOnUiThread { downloadDialog?.setMessage("Downloading ${it.key}") }
                val inputStream = apiConnector.download(it.uri)
                if (inputStream != null) {
                    val outputFile = File(cacheDir, "$code/${it.key}")
                    outputFile.parentFile.mkdirs()
                    outputFile.createNewFile()
                    outputFile.copyInputStreamToFile(inputStream)
                }
            }
            runOnUiThread {
                downloadDialog?.dismiss()
                defaultSharedPreferences().setCode(code)
                navigation.selectedItemId = R.id.navigation_preview
            }
        }
    }

    override fun onCameraPermissionGranted() {
        val fragment = ScannerFragment()
        fragment.listener = this
        supportFragmentManager.beginTransaction()
                .replace(R.id.main_fragment, fragment)
                .commit()
    }

    override fun onScannedCodeExisting() {
        val fragment = ScanResultFragment()
        fragment.listener = this
        supportFragmentManager.beginTransaction()
                .replace(R.id.main_fragment, fragment)
                .commit()
    }

    override fun onCancel() {
        defaultSharedPreferences().setCode(null)
        val permissionFragment = CameraPermissionFragment()
        permissionFragment.permissionListener = this
        supportFragmentManager.replaceMain(permissionFragment)
    }

    override fun onShare(code: String) {
        val activity = this
        val intent = with(Intent(Intent.ACTION_SEND)) {
            type = "text/plain"
            putExtra(Intent.EXTRA_SUBJECT, activity.getString(R.string.share_title))
            putExtra(Intent.EXTRA_TEXT, activity.getString(R.string.share_text).format(code))
        }
        try {
            startActivity(intent)
        } catch (e: Exception) {
            Log.e(this::class.java.simpleName, "Exception while trying to show intent: " + e)
        }
    }

}
