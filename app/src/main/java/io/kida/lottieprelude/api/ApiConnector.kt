package io.kida.lottieprelude.api

import com.google.gson.Gson
import io.kida.lottieprelude.api.model.Descriptor
import okhttp3.*
import java.io.IOException
import java.io.InputStream
import kotlin.coroutines.experimental.suspendCoroutine

/**
 * Created by kida on 08.10.17.
 */

class ApiConnector {
    private val client = OkHttpClient()
    private val apiUrl = "https://prelottie.apps.kida.io"

    suspend fun getDescriptor(id: String): Descriptor? {
        return suspendCoroutine { continuation ->
            val request = Request.Builder()
                    .url("$apiUrl/descriptor/$id")
                    .build()

            client.newCall(request).enqueue(object: Callback {
                override fun onResponse(call: Call?, response: Response?) {
                    response?.let {
                        it.body()?.let {
                            continuation.resume(Gson().fromJson<Descriptor>(it.string(), Descriptor::class.java))
                            return
                        }
                    }
                    continuation.resume(null)
                }

                override fun onFailure(call: Call?, e: IOException?) {
                    continuation.resume(null)
                }

            })
        }
    }

    suspend fun download(uri: String): InputStream? {
        return suspendCoroutine { continuation ->
            val request = Request.Builder()
                    .url(uri)
                    .build()

            client.newCall(request).enqueue(object: Callback {
                override fun onResponse(call: Call?, response: Response?) {
                    response?.let {
                        it.body()?.let {
                            continuation.resume(it.byteStream())
                            return
                        }
                    }
                    continuation.resume(null)
                }

                override fun onFailure(call: Call?, e: IOException?) {
                    continuation.resume(null)
                }

            })
        }
    }
}