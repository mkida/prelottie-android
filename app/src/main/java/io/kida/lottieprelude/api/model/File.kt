package io.kida.lottieprelude.api.model

/**
 * Created by kida on 08.10.17.
 */

data class File(val uri: String, val key: String)