package io.kida.lottieprelude.extension

import java.io.File
import java.io.InputStream

/**
 * Created by kida on 08.10.17.
 */

fun File.copyInputStreamToFile(inputStream: InputStream) {
    inputStream.use { input ->
        this.outputStream().use { fileOut ->
            input.copyTo(fileOut)
        }
    }
}