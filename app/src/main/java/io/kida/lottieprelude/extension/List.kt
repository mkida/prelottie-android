package io.kida.lottieprelude.extension

/**
 * Created by kida on 08.10.17.
 */

inline fun <reified S: Any>List<Any>.filterAndMap(): List<S> {
    return this.filter { it is S }.map { it as S }
}