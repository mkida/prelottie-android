package io.kida.lottieprelude.extension

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import androidx.content.edit

/**
 * Created by kida on 20.02.18.
 */

fun Context.defaultSharedPreferences(): SharedPreferences {
    return getSharedPreferences("prelottie", MODE_PRIVATE)
}

fun SharedPreferences.setCode(code: String?) {
    edit {
        when(code) {
            null -> remove("code")
            else -> putString("code", code)
        }
    }
}

fun SharedPreferences.getCode(): String? {
    return getString("code", null)
}