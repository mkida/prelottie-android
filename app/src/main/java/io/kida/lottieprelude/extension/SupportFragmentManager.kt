package io.kida.lottieprelude.extension

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager

/**
 * Created by kida on 08.10.17.
 */

fun FragmentManager.replaceMain(fragment: Fragment) {
    beginTransaction()
            .replace(io.kida.lottieprelude.R.id.main_fragment, fragment)
            .commit()
}