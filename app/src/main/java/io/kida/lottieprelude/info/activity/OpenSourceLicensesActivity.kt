package io.kida.lottieprelude.info.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.webkit.WebView
import io.kida.lottieprelude.R


/**
 * Created by kida on 16.02.18.
 */

class OpenSourceLicensesActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_open_source_licenses)

        val webView = findViewById<WebView>(R.id.web_view)
        webView.loadUrl("file:///android_asset/open_source_licenses.html")
    }
}