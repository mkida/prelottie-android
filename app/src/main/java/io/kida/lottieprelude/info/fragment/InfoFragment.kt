package io.kida.lottieprelude.info.fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import io.kida.lottieprelude.R
import io.kida.lottieprelude.info.activity.OpenSourceLicensesActivity

/**
 * Created by kida on 10.10.17.
 */

class InfoFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_info, container, false)
    }

    override fun onResume() {
        super.onResume()
        
        val licensesButton = view?.findViewById<Button>(R.id.button_open_source_licenses)
        licensesButton?.setOnClickListener {
            startActivity(Intent(activity, OpenSourceLicensesActivity::class.java))
        }
    }
}