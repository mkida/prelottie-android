package io.kida.lottieprelude.preview.fragment

import android.app.AlertDialog
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.airbnb.lottie.LottieAnimationView
import io.kida.lottieprelude.R
import io.kida.lottieprelude.extension.defaultSharedPreferences
import io.kida.lottieprelude.extension.getCode
import org.json.JSONObject
import java.io.File

/**
 * Created by kida on 08.10.17.
 */

class PreviewFragment: Fragment() {

    private var animationView: LottieAnimationView? = null
    private var scanPlaceholder: TextView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_preview, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        animationView = view.findViewById(R.id.animation_view)
        scanPlaceholder = view.findViewById(R.id.preview_scan_placeholder)
        reloadAnimation()
    }

    private fun reloadAnimation() {
        val code = context?.defaultSharedPreferences()?.getCode()
        when(code) {
            null -> scanPlaceholder?.visibility = View.VISIBLE
            else -> {
                scanPlaceholder?.visibility = View.GONE

                animationView?.let { animationView ->
                    animationView.setImageAssetDelegate { asset ->
                        BitmapFactory.decodeFile("${activity?.cacheDir}/$code/images/${asset.fileName}")
                    }

                    val json = getAnimationJson(code)
                    if (json != null) {
                        animationView.setAnimation(json)
                        animationView.playAnimation()
                    }
                }
            }
        }
    }

    private fun getAnimationJson(code: String, subfolder: String? = null): JSONObject? {
        return try {
            val path = subfolder ?: ""
            val jsonFile = File("${activity?.cacheDir}/$code/$path").list()
                    .first { it.endsWith("json") }

            JSONObject(File("${activity?.cacheDir}/$code/$path/$jsonFile").readText())
        } catch (e: NoSuchElementException) {

            val nextSubfolders = File("${activity?.cacheDir}/$code").list()
                    .filter {
                        File("${activity?.cacheDir}/$code/$it").isDirectory
                    }

            if (nextSubfolders.isEmpty().not()) {
                getAnimationJson(code, nextSubfolders.first())
            } else {
                AlertDialog.Builder(context)
                        .setTitle(R.string.error_title)
                        .setMessage(R.string.error_loading_animation_json)
                        .setNeutralButton(R.string.alert_button_ok, null)
                        .show()
                null
            }
        }
    }
}