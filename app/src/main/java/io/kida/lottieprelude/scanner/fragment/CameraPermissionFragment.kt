package io.kida.lottieprelude.scanner.fragment

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import io.kida.lottieprelude.R
import io.kida.lottieprelude.extension.defaultSharedPreferences
import io.kida.lottieprelude.extension.getCode


/**
 * Created by kida on 07.10.17.
 */

open class CameraPermissionFragment: Fragment() {

    interface Callbacks {
        fun onCameraPermissionGranted()
        fun onScannedCodeExisting()
    }

    val CAMERA_PERMISSION_CODE = 1000

    lateinit var buttonGrant: Button

    var permissionListener: Callbacks? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_camera_permission, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        buttonGrant = view.findViewById<Button>(R.id.button_grant)
        buttonGrant.setOnClickListener {
            if (!checkPermissionAndStart()) {
                ActivityCompat.requestPermissions(activity!!, listOf(Manifest.permission.CAMERA).toTypedArray(), CAMERA_PERMISSION_CODE)
            }
        }
    }

    fun checkPermissionAndStart(): Boolean {
        if (ContextCompat.checkSelfPermission(activity!!, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            when(context?.defaultSharedPreferences()?.getCode()) {
                null -> showScanner()
                else -> showExistingCode()
            }
            return true
        }
        return false
    }

    override fun onResume() {
        super.onResume()
        checkPermissionAndStart()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode) {
            CAMERA_PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults.first() == PackageManager.PERMISSION_GRANTED) {
                    showScanner()
                }
            }
        }
    }

    private fun showScanner() {
        permissionListener?.onCameraPermissionGranted()
    }

    private fun showExistingCode() {
        permissionListener?.onScannedCodeExisting()
    }

    override fun onDestroyView() {
        permissionListener = null
        super.onDestroyView()
    }
}