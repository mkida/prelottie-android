package io.kida.lottieprelude.scanner.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeEncoder
import io.kida.lottieprelude.R
import io.kida.lottieprelude.extension.defaultSharedPreferences
import io.kida.lottieprelude.extension.getCode

/**
 * Created by kida on 07.10.17.
 */

class ScanResultFragment: Fragment() {

    interface Callbacks {
        fun onCancel()
        fun onShare(code: String)
    }

    var listener: Callbacks? = null

    private var imageView: ImageView? = null
    private var buttonCancel: Button? = null
    private var buttonShare: Button? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_scan_result, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imageView = view.findViewById(R.id.imageView)
        buttonCancel = view.findViewById(R.id.buttonCancel)
        buttonCancel?.setOnClickListener {
            listener?.onCancel()
        }

        buttonShare = view.findViewById(R.id.buttonShare)
        buttonShare?.setOnClickListener {
            listener?.onShare(context!!.defaultSharedPreferences().getCode()!!)
        }

        context?.defaultSharedPreferences()?.getCode()?.let { show(it) }
    }

    override fun onDestroyView() {
        listener = null
        super.onDestroyView()
    }

    private fun show(code: String) {
        imageView?.setImageBitmap(
                BarcodeEncoder().encodeBitmap(code, BarcodeFormat.QR_CODE, 640, 640)
        )
    }
}