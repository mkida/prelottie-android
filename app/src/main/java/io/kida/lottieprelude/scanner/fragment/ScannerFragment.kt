package io.kida.lottieprelude.scanner.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.DecoratedBarcodeView
import io.kida.lottieprelude.R

/**
 * Created by kida on 10/7/17.
 */

class ScannerFragment: Fragment(), BarcodeCallback {

    interface Callbacks {
        fun onScanSuccess(code: String)
    }

    lateinit var scannerView: DecoratedBarcodeView
    var listener: Callbacks? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_scanner, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        scannerView = view.findViewById(R.id.scanner_view)
        scannerView.decodeSingle(this)
    }

    override fun onResume() {
        super.onResume()
        scannerView.resume()
    }

    override fun onPause() {
        super.onPause()
        scannerView.pause()
    }

    override fun onDestroyView() {
        listener = null
        super.onDestroyView()
    }

    override fun possibleResultPoints(resultPoints: MutableList<ResultPoint>?) {
        // no-op
    }

    override fun barcodeResult(result: BarcodeResult?) {
        Log.d(this.javaClass.simpleName, "BarcodeResult: ${result.toString()}")
        listener?.onScanSuccess(result.toString())
    }
}