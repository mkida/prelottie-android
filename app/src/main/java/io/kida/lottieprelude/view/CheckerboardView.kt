package io.kida.lottieprelude.view

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View

/**
 * Created by kida on 19.11.17.
 */

class CheckerboardView : View {

     private var paint: Paint

    constructor(context: Context) : super(context) {
        paint = createCheckerBoard(5)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        paint = createCheckerBoard(5)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        paint = createCheckerBoard(5)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        paint = createCheckerBoard(5)
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.drawPaint(paint)
        super.onDraw(canvas)
    }

    private fun createCheckerBoard(pixelSize: Int): Paint {
        val bitmap = Bitmap.createBitmap(pixelSize * 2, pixelSize * 2, Bitmap.Config.ARGB_8888)

        val fill = Paint(Paint.ANTI_ALIAS_FLAG)
        fill.style = Paint.Style.FILL
        fill.color = 0x22000000

        val canvas = Canvas(bitmap)
        val rect = Rect(0, 0, pixelSize, pixelSize)
        canvas.drawRect(rect, fill)
        rect.offset(pixelSize, pixelSize)
        canvas.drawRect(rect, fill)

        val paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.shader = BitmapShader(bitmap, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT)
        return paint
    }
}